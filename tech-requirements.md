# Welcome page (root)

1. Header (transparent - like in [Netflix](https://www.netflix.com))
   1. logo
   2. sign in
2. Welcome screen (like in [Netflix](https://www.netflix.com))
   1. background - grid images of movies posters
   2. title some text
   3. go as guest - button (?)
3. Infinite slide with last shows (like in [hbomax](https://www.hbomax.com))
4. Make your own playlist (go to [screenshots](https://themeforest.net/item/skrn-media-streaming-app/21138330))
5. Watch in any devices (go to [screenshots](https://themeforest.net/item/skrn-media-streaming-app/21138330))
6. FAQ (like in [Netflix](https://www.netflix.com))
   1. what is Dotflix
   2. how much does Dotflix cost
   3. where can i watch
   4. what can i watch on Dotflix
7. Footer (go to [screenshots](https://themeforest.net/item/skrn-media-streaming-app/21138330))
   1. Copyright. All rights reserved. 2021
   2. Social Media (tg, tw, insta)
